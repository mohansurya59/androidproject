package com.test.activity_to_fragment_message;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentPassing extends Fragment {


    Button btn;
    public FragmentPassing() {



    }

    //@Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_passing, container, false);
        btn=(Button)v.findViewById(R.id.fragmentbtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name="welcome";
                MainActivity m= (MainActivity) getActivity();
                m.main(name);
            }
        });



        return v;



    }
}