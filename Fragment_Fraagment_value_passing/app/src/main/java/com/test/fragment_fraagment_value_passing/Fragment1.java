package com.test.fragment_fraagment_value_passing;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment1 extends Fragment {
TextView fname,lname;
Button btn;
    public Fragment1() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {



        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_1, container, false);
        fname=(TextView)v.findViewById(R.id.firstname);
        lname=(TextView)v.findViewById(R.id.lastname);
        btn=(Button)v.findViewById(R.id.fragmentt_one_btn);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String firstname=fname.getText().toString();
                String lastname=lname.getText().toString();
                Bundle bundle = new Bundle();
                bundle.putString("firstname",firstname);
                bundle.putString("lastname",lastname);
                Fragment2 fragment2 = new Fragment2();
                fragment2.setArguments(bundle);
                FragmentManager manager=getActivity().getSupportFragmentManager();
                FragmentTransaction transaction=manager.beginTransaction();
                transaction.replace(R.id.fragment1,fragment2);
                transaction.commit();
            }
        });



        return v;
    }
}
//    Bundle bundle = new Bundle();
//bundle.putString("key","abc"); // Put anything what you want
//
//        Fragment_2 fragment2 = new Fragment_2();
//        fragment2.setArguments(bundle);
//
//        getFragmentManager()
//        .beginTransaction()
//        .replace(R.id.content, fragment2)
//        .commit();
//Button one=(Button)getView().findViewById(R.id.fragmentt_one_btn);
//        one.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Fragment2 fragment2=new Fragment2();
//                FragmentManager manager=getFragmentManager();
//                FragmentTransaction transaction= manager.beginTransaction();
//                transaction.replace(R.id.fragment1,fragment2);
//                transaction.commit();

//            }
//        });