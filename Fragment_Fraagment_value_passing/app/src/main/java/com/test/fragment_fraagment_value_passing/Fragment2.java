package com.test.fragment_fraagment_value_passing;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment2 extends Fragment {

    public Fragment2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v=inflater.inflate(R.layout.fragment_frament2, container, false);
        Bundle bundle=getArguments();
       final String name=bundle.getString("firstname");
       String lastname=bundle.getString("lastname");
        TextView fname,lname;
        Button enter;
        fname=(TextView)v.findViewById(R.id.usernamefragment_two);
        lname=(TextView)v.findViewById(R.id.lastnamefragment_two);
        enter=(Button)v.findViewById(R.id.fragment2enterbtn);
        fname.setText(name);
        lname.setText(lastname);
        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle1=new Bundle();
                bundle1.putString("name",name);
                Intent intent=new Intent(getActivity(),MainActivity.class);
                startActivity(intent);

            }
        });

                return v;
    }
}
