package com.test.test_app;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import java.nio.channels.Channels;

public class Starting_Activity extends AppCompatActivity {
    Handler mHandler = new Handler();
    SharedPreferences sharedPreferences;
    Context context;

    String name=null,pass=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting_);
        mHandler.postDelayed(mUpdateTimeTask, 3000);
    }
    Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            sharedPreferences=getSharedPreferences("LOGINDETAILS",context.MODE_PRIVATE);
            name=sharedPreferences.getString("Username","");
            pass=sharedPreferences.getString("password", "");
            Log.d("message","name  :"+name);
            Log.d("message","pass  :"+pass);
            if( name.trim() != ""  &&  pass.trim() != "" )
            {
                sharedPreferences=getSharedPreferences("LOGINDETAILS",context.MODE_PRIVATE);
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putString("Username",name);
                editor.apply();
                editor.commit();
                Intent intent = new Intent(getApplication(), LogoutActivity.class);
                startActivity(intent);
                finish();
            }
            else if (name == "" || pass == "" )
            {
                Intent intent = new Intent(getApplication(), Login_Activity.class);
                startActivity(intent);
                finish();
            }

        }
    };


}
