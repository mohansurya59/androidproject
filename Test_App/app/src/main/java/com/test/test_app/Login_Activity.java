package com.test.test_app;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class Login_Activity extends AppCompatActivity {
    Button loginbtn;
    EditText username,password;
    CheckBox check;
    SharedPreferences sharedPreferences;
    String name;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_);
        loginbtn=(Button)findViewById(R.id.loginbtn);
        username=(EditText)findViewById(R.id.edit1);
        password=(EditText)findViewById(R.id.passwordtxtbox);
        check=(CheckBox)findViewById(R.id.checkbox);
        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 name=username.getText().toString();
                String pass=password.getText().toString();
                Log.d("name","   : "+name);
                Log.d("pass","   : "+pass);

                //intent.putExtra("name",name);

                if(check.isChecked() && name.trim() !="" && (pass.trim()!="" && pass.length()>4))
                {
                    sharedPreferences=getSharedPreferences("LOGINDETAILS",context.MODE_PRIVATE);
                    SharedPreferences.Editor editor=sharedPreferences.edit();
                    editor.putString("Username",name);
                    editor.putString("password",pass);
                    editor.apply();
                    editor.commit();
                    Intent intent=new Intent(getApplicationContext(),Starting_Activity.class);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(getApplicationContext(),"plz click checkbox and valid username and valid password",Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("EXIT");

        builder.setMessage("ARE YOU LEAVE FROM APP...");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

}
