package com.test.retrofit;

import retrofit.RestAdapter;

public class Api {
    public static ApiInterface getClients(){
        //change base url
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint("http://mobileappdatabase.in/") //Set the Root URL
                .build(); //Finally building the adapter
        //Creating object for our interface
        ApiInterface api = adapter.create(ApiInterface.class);
        return api; // return the APIInterface object

    }
}
