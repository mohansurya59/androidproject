package com.test.lifecycle;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("oncreate","oncreateworking");
        Toast.makeText(getApplicationContext(),"oncreate",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("restart","restartworking");
        Toast.makeText(getApplicationContext(),"ONRESTART",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("resume","resumeworking");
        Toast.makeText(getApplicationContext(),"ONRESUME",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("onstart","onstartworking");
        Toast.makeText(getApplicationContext(),"ONSTART",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("onpause","onpauseworking");
        Toast.makeText(getApplicationContext(),"ONPAUSE",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("onstop","onStopworking");
        Toast.makeText(getApplicationContext(),"ONSTOP",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("ondestory","ondestoryworking");
        Toast.makeText(getApplicationContext(),"ONDESTORY",Toast.LENGTH_SHORT).show();
    }
}
