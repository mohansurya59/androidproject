package com.test.fragment_lifecycle;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class TestFragment extends Fragment {

    public TestFragment() {
        // Required empty public constructor
        Log.d("fragment","openingfragment");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("oncreatevie ","oncreate view working");
        return inflater.inflate(R.layout.fragment_test, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("onActivityCreated","on activity create  is working");
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("onViewCreated Called","on create view is working");

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.d("on attach","on attached  is working");
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("on create","on create  is working");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("on destory","on destory  is working");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("on Destoryview","on destory view is working");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("on detach","on detach  is working");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("on pause","on pause  is working");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("on resume","on resume  is working");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("on start", "on start  is working");
    }
    @Override
    public void onStop() {
        super.onStop();
        Log.d("on stop","on stop  is working");
    }
}
