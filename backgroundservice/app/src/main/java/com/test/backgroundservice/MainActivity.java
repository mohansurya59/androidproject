package com.test.backgroundservice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
private Button startbtn,stopbtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startbtn=findViewById(R.id.startbtn);
        stopbtn=findViewById(R.id.stopbtn);
        startbtn.setOnClickListener(this);
        stopbtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
       if(view==startbtn){
           startService(new Intent(this,MyAndroidService.class));
       }
       else if(view==stopbtn){
           stopService(new Intent(this,MyAndroidService.class));
       }
    }
}
