package com.test.navigation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView bottomnavigation;
    TextView textView;
    Fragment fragment;
    FragmentManager manager;
    FragmentTransaction transaction;
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    ActionBarDrawerToggle toggle;
    private AppBarConfiguration mAppBarConfiguration;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        drawerLayout = findViewById(R.id.drawerlayout);
        navigationView = (NavigationView) findViewById(R.id.navigationview);
        bottomnavigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);

//
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Log.d("msg","welcome");
                int id = item.getItemId();
                switch (id) {
                    case R.id.informations:
//                        Toast.makeText(MainActivity.this, "welcome", Toast.LENGTH_SHORT).show();
                        fragment=new Informations();
                        manager=getSupportFragmentManager();
                        transaction=manager.beginTransaction();
                        transaction.replace(R.id.framelayoutmain,fragment);
                        transaction.commit();
                        break;
                    default:
                        return true;
                }
                return true;
            }
        });

        bottomnavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Log.d("msg1","welcome");
                switch(item.getItemId()){
                    case R.id.home:
                        fragment=new HomeFragment();
                        manager=getSupportFragmentManager();
                       FragmentTransaction transaction=manager.beginTransaction();
                       transaction.replace(R.id.framelayoutmain,fragment);
                        transaction.commit();
                        break;
                    case R.id.setting:
                        fragment=new Setting_fragment();
                        manager=getSupportFragmentManager();
                        transaction=manager.beginTransaction();
                        transaction.replace(R.id.framelayoutmain,fragment);
                        transaction.commit();
                        break;
                    case R.id.profile:
                        fragment=new Profile_Fragment();
                        manager=getSupportFragmentManager();
                        transaction=manager.beginTransaction();
                        transaction.replace(R.id.framelayoutmain,fragment);
                        transaction.commit();
                        break;
                }
                return true;
            }
        });
    }
  //  its use do enable toolbar icon side view on touch
        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            if (toggle.onOptionsItemSelected(item)){
                return true;
        }
            return super.onOptionsItemSelected(item);
        }

}

