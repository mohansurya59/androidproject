package com.test.sample_sidenavigation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity {
    private AppBarConfiguration appBarConfiguration;
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Fragment fragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        drawerLayout = findViewById(R.id.drawlayout);
        navigationView = findViewById(R.id.navigation);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.content_framelayout,
                    new HomeFragment()).commit();
            navigationView.setCheckedItem(R.id.navigation);
        }
//        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                switch (item.getItemId()){
//                    case R.id.about:
//                        fragment=new Aboutfragment();
//                        FragmentManager manager=getSupportFragmentManager();
//                       FragmentTransaction transaction= manager.beginTransaction();
//                       transaction.replace(R.id.content_framelayout,fragment);
//                       transaction.commit();
//                }
//
//                return true;
//            }
//        });

    }
}
//    Toolbar toolbar=findViewById(R.id.toolbar);
//    setSupportActionBar(toolbar);
//    DrawerLayout drawerLayout=findViewById(R.id.drawlayout);
//    NavigationView navigationView=findViewById(R.id.navigation);
//        appBarConfiguration=new AppBarConfiguration.Builder(
//                R.id.home, R.id.about, R.id.gallary)
//                .setDrawerLayout(drawerLayout)
//                .build();
//                NavController navController = Navigation.findNavController(this, R.id.content_framelayout);
//                NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
//                NavigationUI.setupWithNavController(navigationView, navController);
//                }
//
//@Override
//public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_items, menu);
//        return true;
//        }
//
//@Override
//public boolean onSupportNavigateUp() {
//        NavController navController = Navigation.findNavController(this, R.id.content_framelayout);
//        return NavigationUI.navigateUp(navController, appBarConfiguration)
//        || super.onSupportNavigateUp();
//        }