package com.example.sharedpreference;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            Thread.sleep(3000);
            SharedPreferences sp = getSharedPreferences("Logindetails", Context.MODE_PRIVATE);
            String mail = sp.getString("Email", " ");
            String password=sp.getString("Password"," ");
            System.out.println("welcome");
            System.out.println(mail);
            System.out.println(password);
            //System.out.println(mail);
            if (mail == null && password== null) {
                Intent i = new Intent(MainActivity.this, login.class);
                startActivity(i);
                finish();
            }
            else if(mail != null && password != null) {
                Intent i = new Intent(MainActivity.this, navigation.class);
                startActivity(i);
                finish();
            }
        } catch (Exception e) {

        }
    }
}

