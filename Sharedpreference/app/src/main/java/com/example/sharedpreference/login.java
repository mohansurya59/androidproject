package com.example.sharedpreference;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class login extends AppCompatActivity {

   private EditText emailtxt, passwordtxt;
   private CheckBox rememeber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        emailtxt = (EditText) findViewById(R.id.usernameEditText);
        passwordtxt = (EditText) findViewById(R.id.passwordEditText);
        rememeber=(CheckBox) findViewById(R.id.checkBox);
        passwordtxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_NULL) {

                    return true;
                }
                return false;
            }
        });
        Button loginbutton = (Button) findViewById(R.id.loginbtn);
        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
        CheckBox rememeber = (CheckBox) findViewById(R.id.checkBox);
        if (!new PrefManager(this).isUserLogedOut()) {
            //user's email and password both are saved in preferences
            startHomeActivity();
        }


    }
        private void attemptLogin() {
            emailtxt.setError(null);
            passwordtxt.setError(null);

            String email = emailtxt.getText().toString();
            String password = passwordtxt.getText().toString();

            boolean cancel = false;
            View focusView = null;

            // Check for a valid password, if the user entered one.
            if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
                passwordtxt.setError(getString(R.string.error_invalid_password));
                focusView = passwordtxt;
                cancel = true;
            }
            // Check for a valid email address.
            if (TextUtils.isEmpty(email)) {
                emailtxt.setError(getString(R.string.error_field_required));
                focusView = emailtxt;
                cancel = true;
            } else if (!isEmailValid(email)) {
                emailtxt.setError(getString(R.string.error_invalid_email));
                focusView = emailtxt;
                cancel = true;
            }
            if (cancel) {
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();
            } else {
                // save data in local shared preferences
                if (rememeber.isChecked())
                    saveLoginDetails(email, password);
                startHomeActivity();
            }
        }
            private void startHomeActivity()
            {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
            }

            private void saveLoginDetails(String email, String password) {
            new PrefManager(this).savedLoginDetails(email, password);
            }

            private boolean isEmailValid(String email) {
            //TODO: Replace this with your own logic
            return email.contains("@");
            }

            private boolean isPasswordValid(String password) {
            //TODO: Replace this with your own logic
            return password.length() > 4;
            }


}
