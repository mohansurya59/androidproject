package com.test.intent_service;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
private EditText text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text=findViewById(R.id.edittext);
    }
    public void startservice(View v){
        String input=text.getText().toString();
        Intent serviceintent=new Intent(this,ExampleIntentService.class);
        serviceintent.putExtra("inputExtra",input);

        // using all feature context--ContextCompat
        ContextCompat.startForegroundService(this,serviceintent);

    }
}
