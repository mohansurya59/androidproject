package com.test.intent_service;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Notification;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

public class ExampleIntentService extends IntentService {

    private  static final String tag="exampleIntentService";
    private static final String CHANNEL_ID = "CHANNEL_ID";

    private PowerManager.WakeLock wakeLock;
    public ExampleIntentService() {
        super("exampleIntentService");
        setIntentRedelivery(true);
    }

    @SuppressLint("InvalidWakeLockTag")
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(tag,"onCreate method invoking");
        PowerManager powerManager=(PowerManager)getSystemService(POWER_SERVICE);
        wakeLock=powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"Example wakeLock");
        wakeLock.acquire();
        Log.d(tag,"wakelock acquired");
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            Notification notification=new NotificationCompat.Builder(this,CHANNEL_ID)
                    .setContentTitle("Example intent service")
                    .setContentText("Running...")
                    .setSmallIcon(R.drawable.ic_launcher_background)
                    .build();
            startForeground(1,notification);
        }
    }
      @Override
    protected void onHandleIntent(@Nullable Intent intent) {
    Log.d(tag,"on handle indent");
    String input=intent.getStringExtra("inputExtra");
    for(int i=0;i<10;i++){
        Log.d(tag,input+"  -  "+i);
        SystemClock.sleep(1000);
    }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(tag,"ondestory   ...");
        wakeLock.release();
        Log.d(tag,"wakelock relased");
    }
}
